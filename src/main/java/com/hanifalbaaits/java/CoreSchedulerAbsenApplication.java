package com.hanifalbaaits.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.hanifalbaaits.java.config.FileProperties;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties({
	FileProperties.class
})
public class CoreSchedulerAbsenApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoreSchedulerAbsenApplication.class, args);
	}

}
