package com.hanifalbaaits.java.util;

import java.util.List;

public class ResponList {
	
	private String code;
	private List<?> data;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<?> getData() {
		return data;
	}
	public void setData(List<?> data) {
		this.data = data;
	}
}
