package com.hanifalbaaits.java.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class CustomRespon {
	
	public String customRespon(String code, String pesan) {
		JSONObject custom = new JSONObject();
		custom.put("code", code);
		custom.put("data", pesan);
		return custom.toString();
	}
}
