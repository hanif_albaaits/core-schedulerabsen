package com.hanifalbaaits.java.util;

public class FileStorage extends RuntimeException {
	
	    public FileStorage(String message) {
	        super(message);
	    }

	    public FileStorage(String message, Throwable cause) {
	        super(message, cause);
	    }
	}


