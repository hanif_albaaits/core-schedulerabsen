package com.hanifalbaaits.java.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hanifalbaaits.java.model.AbsenCuti;
import com.hanifalbaaits.java.repository.AbsenCutiRepo;

@Service
public class AbsenCutiService {
	
	@Autowired
	AbsenCutiRepo absenCutiRepo;
	
	public List<AbsenCuti> findAll(){
		return absenCutiRepo.findAll();
	}
	
	public List<AbsenCuti> findCutiDate(int id, Date tgl){
		return absenCutiRepo.findCutiDate(id,tgl);
	}
	
	public void save(AbsenCuti data) {
		this.absenCutiRepo.save(data);
	}
	
	public void delete(AbsenCuti data) {
		this.absenCutiRepo.delete(data);
	}
}
