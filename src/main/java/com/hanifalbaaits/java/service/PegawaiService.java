package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hanifalbaaits.java.model.Pegawai;
import com.hanifalbaaits.java.repository.PegawaiRepo;

@Service
public class PegawaiService {
	
	@Autowired
	PegawaiRepo pegawaiRepo;
	
	public List<Pegawai> findAll(){
		return pegawaiRepo.findAll();
	}
	
	public List<Pegawai> findAllActive(){
		return pegawaiRepo.findAllActive();
	}
	
	public List<Pegawai> findPegawai(String param){
		return pegawaiRepo.findPegawai(param);
	}
	
	public List<Pegawai> findAllActiveByNIK(String nik){
		return pegawaiRepo.findAllActiveByNIK(nik);
	}
	
	public void save(Pegawai data) {
		this.pegawaiRepo.save(data);
	}
	
	public void delete(Pegawai data) {
		this.pegawaiRepo.delete(data);
	}
}
