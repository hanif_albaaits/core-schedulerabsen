package com.hanifalbaaits.java.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hanifalbaaits.java.model.AbsenTest;
import com.hanifalbaaits.java.repository.AbsenTestRepo;

@Service
public class AbsenTestService {

	@Autowired
	AbsenTestRepo absenRepo;
	
	public List<AbsenTest> findAll(){
		return absenRepo.findAll();
	}
	
	public AbsenTest findAbsenDate(int id,Date tanggal){
		return absenRepo.findAbsenDate(id,tanggal);
	}
	
	public void save(AbsenTest data) {
		this.absenRepo.save(data);
	}
	
	public void delete(AbsenTest data) {
		this.absenRepo.delete(data);
	}

}
