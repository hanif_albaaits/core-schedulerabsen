package com.hanifalbaaits.java.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hanifalbaaits.java.model.AbsenLembur;
import com.hanifalbaaits.java.repository.AbsenLemburRepo;

@Service
public class AbsenLemburService {

	@Autowired
	AbsenLemburRepo absenLemburRepo;
	
	public List<AbsenLembur> findAll(){
		return absenLemburRepo.findAll();
	}
	
	public List<AbsenLembur> findLemburYesterday(Date tanggal){
		return absenLemburRepo.findLemburYesterday(tanggal);
	}
	
	public void save(AbsenLembur data) {
		this.absenLemburRepo.save(data);
	}
	
	public void delete(AbsenLembur data) {
		this.absenLemburRepo.delete(data);
	}

}
