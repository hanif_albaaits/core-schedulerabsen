package com.hanifalbaaits.java.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import com.hanifalbaaits.java.model.HariMerah;
import com.hanifalbaaits.java.repository.HariMerahRepo;

@Service
public class HariMerahService {
	
	@Autowired
	HariMerahRepo harimerahRepo;
	
	public List<HariMerah> findAll(){
		return harimerahRepo.findAll();
	}
	
	public HariMerah findById(int id) {
		return harimerahRepo.findById(id);
	}
	
	public List<HariMerah> findByKode(String kode) {
		return harimerahRepo.findByKode(kode);
	}
	
	public HariMerah findLast() {
		return harimerahRepo.findLast();
	}
	
	public List<HariMerah> findByDate(Date start, Date end){
		return harimerahRepo.findByDate(start,end);
	}
	
	public List<HariMerah> grupByKodeAndDate(Date start, Date end){
		return harimerahRepo.grupByKodeAndDate(start,end);
	}
	
	public List<HariMerah> grupByKodeAndStartDate(Date start){
		return harimerahRepo.grupByKodeAndStartDate(start);
	}
	
	public List<HariMerah> findByToday(Date dt){
		return harimerahRepo.findByToday(dt);
	}
	
	public void save(HariMerah data) {
		this.harimerahRepo.save(data);
	}
	
	public void delete(HariMerah data) {
		this.harimerahRepo.delete(data);
	}
	
	public void deleteByKode(String kode) {
		this.harimerahRepo.deleteByKode(kode);
	}
}
