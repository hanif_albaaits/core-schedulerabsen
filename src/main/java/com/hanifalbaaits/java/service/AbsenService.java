package com.hanifalbaaits.java.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hanifalbaaits.java.model.Absen;
import com.hanifalbaaits.java.repository.AbsenRepo;

@Service
public class AbsenService {

	@Autowired
	AbsenRepo absenRepo;
	
	public List<Absen> findAll(){
		return absenRepo.findAll();
	}
	
	public Absen findAbsenDate(int id,Date tanggal){
		return absenRepo.findAbsenDate(id,tanggal);
	}
	
	public void save(Absen data) {
		this.absenRepo.save(data);
	}
	
	public void delete(Absen data) {
		this.absenRepo.delete(data);
	}

}
