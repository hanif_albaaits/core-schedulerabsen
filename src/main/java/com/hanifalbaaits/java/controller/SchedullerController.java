package com.hanifalbaaits.java.controller;

import java.sql.Date;
//import java.sql.Time;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.client.ClientAPI;
import com.hanifalbaaits.java.model.Absen;
import com.hanifalbaaits.java.model.AbsenTest;
import com.hanifalbaaits.java.model.AbsenCuti;
import com.hanifalbaaits.java.model.AbsenLembur;
import com.hanifalbaaits.java.model.Pegawai;
import com.hanifalbaaits.java.service.AbsenCutiService;
import com.hanifalbaaits.java.service.AbsenLemburService;
import com.hanifalbaaits.java.service.AbsenService;
import com.hanifalbaaits.java.service.AbsenTestService;
import com.hanifalbaaits.java.service.PegawaiService;
import com.hanifalbaaits.java.util.CustomRespon;

@Component
@RestController
@RequestMapping(value="/api/scheduler")
public class SchedullerController {

	private static Logger logger = LoggerFactory.getLogger(SchedullerController.class);
	
	static final long MILLIS_IN_A_DAY = 1000*60*60*24;
	
	@Autowired
	PegawaiService servicePegawai;
	
	@Autowired
	AbsenService serviceAbsen;
	
	@Autowired
	AbsenTestService serviceAbsenTest;
	
	@Autowired
	AbsenLemburService serviceLembur;
	
	@Autowired
	AbsenCutiService serviceCuti;
	
	@Autowired
	CustomRespon responC;
	
	@Autowired
	ClientAPI clientAPI;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/search/{param}")
	public ResponseEntity<?> searchByParam(@PathVariable String param) {
		String respon = "";
		try {
			
			logger.info("Mencari pegawai dengan: "+param);
			List<Pegawai> pegawai = servicePegawai.findPegawai(param);
			
			if (pegawai == null) {
				logger.info("Pegawai with param "+param+" not found");
				return new ResponseEntity<String>("Pegawai with param "+param+" not found",HttpStatus.BAD_REQUEST);
			}	
			return new ResponseEntity<List<Pegawai>>(pegawai, HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
		}
		respon = responC.customRespon("500", "Internal Server Problem");
		return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value="/createAbsen")
	public ResponseEntity<?> createNewAbsen() throws Exception,ParseException {
		String respon = "";
		try {
			
			logger.info("create new absensi today");
			long millis=System.currentTimeMillis(); 
			Date now = new Date(millis);
			logger.info("membuat absensi tanggal: "+now);
			List<Pegawai> allPegawai = servicePegawai.findAllActive();
			logger.info("total pegawai aktive : "+allPegawai.size());
			int terbuat = 0; int sudah_terbuat = 0; int gagal_terbuat = 0;
			if(!allPegawai.isEmpty()) {
				for(Pegawai pegawai : allPegawai) {
					logger.info("cek sudah ada belum. id:"+pegawai.getId()+" nama pegawai: "+pegawai.getNama());
					try {
						
						Absen absen = serviceAbsen.findAbsenDate(pegawai.getId(),now); //cek apakah data sudah ada
						if(absen==null) {
							terbuat++;
							createAbsenCekCuti(pegawai.getId(),now); //cek di tabel cuti jika ada tambahkan cuti
						} else {
							sudah_terbuat++;
							logger.info("sudah ada data hari ini di absen. no id peg: "+pegawai.getId());
						}
						
					} catch (Exception e) {
						// TODO: handle exception
						gagal_terbuat++;
						logger.info("error, mungkin ada data 2.");
						logger.info("e: "+e.getStackTrace());
					}
				}
			}
			String bls = "Total pegawai aktive : "+allPegawai.size()+
					" Absensi Terbuat : "+terbuat+
					" Absensi Sudah Terbuat : "+sudah_terbuat+
					" Absensi Gagal Terbuat : "+gagal_terbuat;
			logger.info("Respon: "+bls);
			respon = responC.customRespon("200",bls);
			return new ResponseEntity(respon, HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);
		}
		respon = responC.customRespon("500", "Internal Server Problem");
		return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	public void checkAbsenLembur() throws Exception, ParseException {
		logger.info("check Absen Lembur Yesterday");
		try {
			
			long millis=System.currentTimeMillis(); 
			Date now = new Date(millis - MILLIS_IN_A_DAY);
			//Date now = new Date(millis);
			logger.info("tanggal: "+now);
			List<AbsenLembur> all = serviceLembur.findLemburYesterday(now);
			
			if(!all.isEmpty()) {
				for (AbsenLembur data : all) {
					logger.info("id_lembur  : "+data.getId_lembur());
					logger.info("nik pegawai: "+data.getNik());
					logger.info("nik atasan : "+data.getNik_atasan());
					logger.info("send to api absen -->");
					
					
					String respon = sendtoNewAbsen(String.valueOf(data.getId_lembur()),data.getNik());
//					String respon = sendtoAbsen(String.valueOf(data.getId_lembur()), data.getNik(), data.getNik_atasan());
//					String respon1 = sendtoAbsen1(String.valueOf(data.getId_lembur()), data.getNik(), data.getNik_atasan());
					
					logger.info("respon : "+respon);
//					logger.info("respon1: "+respon1);
				}
				logger.info("total data lembur: "+all.size());
				return;
			} else {
				logger.info("data kosong - tidak ada yang lembur semalam");
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);
		}
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value="/createAbsen-test")
	public ResponseEntity<?> createNewAbsenTestRIO() throws Exception,ParseException {
		String respon = "";
		try {
			
			logger.info("create test rio new absensi today");
			long millis=System.currentTimeMillis(); 
			Date now = new Date(millis);
			logger.info("tanggal: "+now);
			List<Pegawai> allPegawai = servicePegawai.findAllActive();
			if(!allPegawai.isEmpty()) {
				for(Pegawai pegawai : allPegawai) {
					//logger.info("Pegawai: "+pegawai.getNama());
					AbsenTest absen = serviceAbsenTest.findAbsenDate(pegawai.getId(),now); //cek apakah data sudah ada
					if(absen==null) {
						createAbsenCekCutiTestRIO(pegawai.getId(),now); //cek di tabel cuti jika ada tambahkan cuti
					} else {
						logger.info("sudah ada data hari ini di absen. no id peg: "+pegawai.getId());
					}
				}
			}
			respon = responC.customRespon("200", "Absensi terbuat : "+allPegawai.size()+" data karyawan aktif");
			return new ResponseEntity(respon, HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);
		}
		respon = responC.customRespon("500", "Internal Server Problem");
		return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public void createAbsenCekCuti(int id, Date tgl) {
		logger.info("cek cuti");
		try {
			List<AbsenCuti> cuti = serviceCuti.findCutiDate(id,tgl);
			//Time zero = Time.valueOf("00:00:00");
			if (cuti.size() > 0) {
				logger.info("cuti. create absen ket: cuti");
				Absen absenToday = new Absen();
				absenToday.setId_pegawai(id);
				absenToday.setTanggal_in(tgl);
				//absenToday.setAbsen_in(zero);
				absenToday.setCaption_in("cuti");
				absenToday.setKeterangan("c");
				serviceAbsen.save(absenToday);
				
			} else {
				logger.info("tidak ada cuti. create absen ket: alfa");
				Absen absenToday = new Absen();
				absenToday.setId_pegawai(id);
				absenToday.setTanggal_in(tgl);
				//absenToday.setAbsen_in(zero);
				absenToday.setKeterangan("a");
				serviceAbsen.save(absenToday);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("ERROR cek cuti. id_pegawai: "+id);
			logger.info("LOGGER CEK CUTI ERROR");
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);
		}
		
	}
	
	//FUNCTION SUDAH TIDAK TERPAKAI LAGI, waktu itu rio lagi riset suruh buat tb_absen_test
	public void createAbsenCekCutiTestRIO(int id, Date tgl) {
		logger.info("cek cuti test rio");
		try {
			
			List<AbsenCuti> cuti = serviceCuti.findCutiDate(id,tgl);
			//Time zero = Time.valueOf("00:00:00");
			
			if (cuti!=null) {
				logger.info("cuti. create absen ket: cuti");
				AbsenTest absenToday = new AbsenTest();
				absenToday.setId_pegawai(id);
				absenToday.setTanggal_in(tgl);
				//absenToday.setAbsen_in(zero);
				absenToday.setCaption_in("cuti");
				absenToday.setKeterangan("c");
				serviceAbsenTest.save(absenToday);
				
			} else {
				logger.info("tidak ada cuti. create absen ket: alfa");
				AbsenTest absenToday = new AbsenTest();
				absenToday.setId_pegawai(id);
				absenToday.setTanggal_in(tgl);
				//absenToday.setAbsen_in(zero);
				absenToday.setKeterangan("a");
				serviceAbsenTest.save(absenToday);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);
		}
		
	}
	
	@Async
	public String sendtoAbsen(String id, String nik, String nik_atasan) {
		return clientAPI.sendtoAbsen(id,nik,nik_atasan);
	}
	
	@Async
	public String sendtoAbsen1(String id, String nik, String nik_atasan) {
		return clientAPI.sendtoAbsen1(id,nik,nik_atasan);
	}
	
	@Async
	public String sendtoNewAbsen(String id, String nik) {
		return clientAPI.sendtoNewAbsen(id,nik);
	}
}
