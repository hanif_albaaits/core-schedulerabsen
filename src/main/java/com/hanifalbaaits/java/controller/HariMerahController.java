package com.hanifalbaaits.java.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.hanifalbaaits.java.model.HariMerah;
import com.hanifalbaaits.java.model.HariMerahResult;
import com.hanifalbaaits.java.service.HariMerahService;
import com.hanifalbaaits.java.service.UploadService;
import com.hanifalbaaits.java.util.CustomRespon;
import com.hanifalbaaits.java.util.ResponList;
import com.hanifalbaaits.java.util.ResponObject;


@RestController
@RequestMapping(value="/api/scheduler")
public class HariMerahController {

	private Logger logger = LoggerFactory.getLogger(HariMerahController.class);

	@Autowired
	HariMerahService serviceHari;
	
	@Autowired
	CustomRespon responC;
	
	@Autowired
	UploadService serviceUpload;
	
	@Value("${file.upload-dir}")
	String path_file;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/harimerah")
	public ResponseEntity<?> getHariMerah() throws Exception {
		
		String respon = "";
		try {
			
			Date today = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy"); 
			String year = dateFormat.format(today);
			
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, Integer.parseInt(year));
			cal.set(Calendar.DAY_OF_YEAR, 1);  
			cal.set(Calendar.HOUR_OF_DAY, 0);
			Date start = cal.getTime();

			//set date to last day of 2014
			cal.set(Calendar.YEAR, Integer.parseInt(year));
			cal.set(Calendar.MONTH, 12); // 11 = december
			cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve

			Date end = cal.getTime();
			
			logger.info("date start parse "+start.toString());
			logger.info("date end parse "+end.toString());
				
			List<HariMerah> group = serviceHari.grupByKodeAndStartDate(start);
			List<HariMerahResult> result = new ArrayList<HariMerahResult>();
			
			for (HariMerah hariMerah : group) {
				
				List<HariMerah> harim = serviceHari.findByKode(hariMerah.getKode());
				logger.info("hari: "+hariMerah.getTentang());
				logger.info("date: "+hariMerah.getTanggal().toString());
				int i = 0;
				HariMerahResult hr = new HariMerahResult();
				
				for (HariMerah harimm : harim) {
					
					if(i==0) {
						hr.setKode(harimm.getKode());
						hr.setTentang(harimm.getTentang());
						hr.setDescription(harimm.getDescription());
						hr.setTanggal_mulai(configGetDate.format(harimm.getTanggal()));
						hr.setTanggal_selesai(configGetDate.format(harimm.getTanggal()));
						
					} else if (i == harim.size() -1) {
						
						hr.setTanggal_selesai(configGetDate.format(harimm.getTanggal()));
						
					} else {
						
					}
					
					i++;
				}
				result.add(hr);
			}
			
			ResponList data = new ResponList();
			data.setCode("1");
			data.setData(result);
			return new ResponseEntity(data,HttpStatus.OK);
					
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
		}
		respon = responC.customRespon("500", "Internal Server Problem");
		return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);	
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@GetMapping(value="/harimerah/{id}")
	public ResponseEntity<?> getHariMerah(
			@PathVariable int id) throws Exception {
		
		String respon = "";
		HariMerah pic = serviceHari.findById(id);
				
		if(pic != null) {
			try {
				
				logger.info("GET PIC : "+id);	
				ResponObject data = new ResponObject();
				data.setCode("1");
				data.setData(pic);
				return new ResponseEntity(data,HttpStatus.OK);
				
			} catch (Exception e) {
				// TODO: handle exception
				logger.error(e.toString());
				logger.error(this.getClass().toGenericString(), e);	
				respon = responC.customRespon("500", "Internal Server Problem");
				return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		} else {
			respon = responC.customRespon("0", "Data not found");
			return new ResponseEntity(respon,HttpStatus.OK);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@GetMapping(value="/harimerah/kode/{kd}")
	public ResponseEntity<?> getHariMerahKode(
			@PathVariable String kd) throws Exception {
		
		String respon = "";
		List<HariMerah> pic = serviceHari.findByKode(kd);
				
		try {
			
			logger.info("GET kode : "+kd);	
			ResponList data = new ResponList();
			data.setCode("1");
			data.setData(pic);
			return new ResponseEntity(data,HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
			respon = responC.customRespon("500", "Internal Server Problem");
			return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@GetMapping(value="/harimerah/today")
	public ResponseEntity<?> getHariMerahToday() throws Exception {
		
		String respon = "";
		Date today = new Date();
		logger.info("today "+today.toString());
		List<HariMerah> pic = serviceHari.findByToday(today);
				
		if(pic.size()>0) {
			try {
					
				ResponList data = new ResponList();
				data.setCode("1");
				data.setData(pic);
				return new ResponseEntity(data,HttpStatus.OK);
				
			} catch (Exception e) {
				// TODO: handle exception
				logger.error(e.toString());
				logger.error(this.getClass().toGenericString(), e);	
				respon = responC.customRespon("500", "Internal Server Problem");
				return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		} else {
			respon = responC.customRespon("0", "Sekarang bukan hari libur");
			return new ResponseEntity(respon,HttpStatus.OK);
		}
	}
		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value="/harimerah/search")
	public ResponseEntity<?> getHariMerahSearch(@RequestBody Map<String, String> body) throws Exception {
		
		String respon = "";
		try {
			
		logger.info("startdate "+body.get("startdate"));
		logger.info("enddate "+body.get("enddate"));
		
		if (body.toString()==null) return new ResponseEntity(responC.customRespon("400", "Body null"),HttpStatus.BAD_REQUEST);
		if (body.get("startdate")==null || body.get("startdate").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need startdate field"),HttpStatus.BAD_REQUEST);
		if (body.get("enddate")==null || body.get("enddate").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need enddate field"),HttpStatus.BAD_REQUEST);
		
		String startdate = body.get("startdate");
		String enddate = body.get("enddate");
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = df.parse(startdate);
		Date date2 = df.parse(enddate);
		
		logger.info("date1 parse "+date1.toString());
		logger.info("date2 parse "+date2.toString());
			
		List<HariMerah> hari = serviceHari.findByDate(date1, date2);
		ResponList data = new ResponList();
		data.setCode("1");
		data.setData(hari);
	
		return new ResponseEntity(data,HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
		}
		respon = responC.customRespon("500", "Internal Server Problem");
		return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value="/harimerah")
	public ResponseEntity<?> createHariMerah(@RequestBody Map<String,String> body) throws Exception {
		
		if (body.toString()==null) return new ResponseEntity(responC.customRespon("400", "Body null"),HttpStatus.BAD_REQUEST);
		if (body.get("tentang")==null || body.get("tentang").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need tentang field"),HttpStatus.BAD_REQUEST);
		if (body.get("description")==null || body.get("description").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need description field"),HttpStatus.BAD_REQUEST);
		if (body.get("startdate")==null || body.get("startdate").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need startdate field"),HttpStatus.BAD_REQUEST);
		if (body.get("enddate")==null || body.get("enddate").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need enddate field"),HttpStatus.BAD_REQUEST);
		
		try {
			logger.info("CREATE HARIMERAH");
			String startdate = body.get("startdate");
			String enddate = body.get("enddate");
			String tentang = body.get("tentang");
			String description = body.get("description");
			String kd = "";
			
			logger.info("tentang : "+tentang);
			logger.info("description : "+description);
			logger.info("startdate : "+startdate);
			logger.info("enddate : "+enddate);
			
			configInputDate.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date date1 = configInputDate.parse(startdate);
			Date date2 = configInputDate.parse(enddate);
	
			HariMerah last = serviceHari.findLast();
			
			if(last == null) {
				kd = "LB00001";
			} else {
				int no = Integer.parseInt(last.getKode().substring(2));
				no = no + 1;
				
				if(String.valueOf(no).length() == 5 ) {
					kd = "LB" + no;
				} else if(String.valueOf(no).length() == 4 ) {
					kd = "LB0" + no;
				} else if(String.valueOf(no).length() == 3 ) {
					kd = "LB00" + no;
				} else if(String.valueOf(no).length() == 2 ) {
					kd = "LB000" + no;
				} else {
					kd = "LB0000" + no;
				}
			}
			
			int durasi = calculateNumberOfDaysBetween(date1, date2);
			logger.info("durasi start and end : "+durasi);
			
			List<HariMerah> resp =  new ArrayList<HariMerah>();
			
			for (int i = 0; i < durasi; i++) {
				
				Calendar c = Calendar.getInstance(); 
				c.setTime(date1); 
				c.add(Calendar.DATE, i);
				
				HariMerah createHari = new HariMerah();
				createHari.setKode(kd);
				createHari.setTentang(tentang);
				createHari.setDescription(description);
				createHari.setTanggal(c.getTime());
				this.serviceHari.save(createHari);
				
				resp.add(createHari);
			}
			
			ResponList data = new ResponList();
			data.setCode("1");
			data.setData(resp);
			return new ResponseEntity(data,HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
		}
		
		String respon = responC.customRespon("500", "Internal Server Problem");
		return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PutMapping(value="/harimerah/{kode}")
	public ResponseEntity<?> updateHariMerah(@PathVariable String kode, @RequestBody Map<String,String> body) throws Exception {
		
		if (body.toString()==null) return new ResponseEntity(responC.customRespon("400", "Body null"),HttpStatus.BAD_REQUEST);
		if (body.get("tentang")==null || body.get("tentang").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need tentang field"),HttpStatus.BAD_REQUEST);
		if (body.get("description")==null || body.get("description").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need description field"),HttpStatus.BAD_REQUEST);
		if (body.get("startdate")==null || body.get("startdate").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need startdate field"),HttpStatus.BAD_REQUEST);
		if (body.get("enddate")==null || body.get("enddate").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need enddate field"),HttpStatus.BAD_REQUEST);
		
		try {
			logger.info("UPDATE HARIMERAH");
			logger.info("kode : "+kode);
			String startdate = body.get("startdate");
			String enddate = body.get("enddate");
			String tentang = body.get("tentang");
			String description = body.get("description");
			
			logger.info("tentang : "+tentang);
			logger.info("description : "+description);
			logger.info("startdate : "+startdate);
			logger.info("enddate : "+enddate);
			
			configInputDate.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date date1 = configInputDate.parse(startdate);
			Date date2 = configInputDate.parse(enddate);
			
			int durasi = calculateNumberOfDaysBetween(date1, date2);
			logger.info("durasi start and end : "+durasi);
			
			this.serviceHari.deleteByKode(kode);
			logger.info("delete all by kode");
			List<HariMerah> resp =  new ArrayList<HariMerah>();
			for (int i = 0; i < durasi; i++) {
				
				Calendar c = Calendar.getInstance(); 
				c.setTime(date1); 
				c.add(Calendar.DATE, i);
				
				HariMerah createHari = new HariMerah();
				createHari.setKode(kode);
				createHari.setTentang(tentang);
				createHari.setDescription(description);
				createHari.setTanggal(c.getTime());
				this.serviceHari.save(createHari);
				
				resp.add(createHari);
			}
			
			ResponList data = new ResponList();
			data.setCode("1");
			data.setData(resp);
			return new ResponseEntity(data,HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
		}
		
		String respon = responC.customRespon("500", "Internal Server Problem");
		return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@DeleteMapping(value="/harimerah/{kode}")
	public ResponseEntity<?> deleteHariMerah(@PathVariable String kode) throws Exception {
		
		try {				
			logger.info("DELETE API Harimerah");
			logger.info("Kode Harimerah: "+kode);
			
			this.serviceHari.deleteByKode(kode);
			
			String respon = responC.customRespon("1", "Success delete Harimerah : "+kode);
			return new ResponseEntity(respon,HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
		}
		
		String respon = responC.customRespon("500", "Internal Server Problem");
		return new ResponseEntity(respon,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "null" })
	@PostMapping(value="/harimerah/upload")
	public ResponseEntity<?> hariUpload(
		@RequestParam Map<String, String> body,
		@RequestParam("file") MultipartFile file
		) throws Exception, ParseException {
	
		logger.info("CREATE UPLOAD FILE: "+body.toString());
		logger.info("FILE name: "+file.getOriginalFilename()+" TYPE: "+file.getContentType());
		
		//try untuk baca extensi file harus CSV
		try {
			String split [] = file.getOriginalFilename().split("\\.");
			if(!split[split.length-1].toLowerCase().contentEquals("csv")) {
				logger.info("validasi => File type must CSV");
					return new ResponseEntity(responC.customRespon("400", "File type must CSV"),HttpStatus.BAD_REQUEST);	
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.error(e.toString());
				logger.error(this.getClass().toGenericString(), e);	
			}
		logger.info("Lolos validasi file csv");
		if (body.toString()==null) return new ResponseEntity<String>("request body is missing", HttpStatus.BAD_REQUEST);
		if (body.get("tahun")==null || body.get("tahun").toString().isEmpty()) return new ResponseEntity(responC.customRespon("400", "Need tahun field"),HttpStatus.BAD_REQUEST);
		
		try {
			logger.info("UPLOAD FILE untuk RKA");
			String tahun = body.get("tahun");
			logger.info("UPLOAD-FILE tahun: "+tahun);
			
			Date today = new Date();
			String filename = "upload_tglmerah"+"_"+today.getTime()+"_"+tahun+".csv";
			
			String uripath = serviceUpload.storeFile(file,filename);		 
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
			.path("/download_file/")
			.path(uripath)
			.toUriString();
			
			logger.info("UPLOAD-FILE uripath: "+uripath);
			logger.info("UPLOAD-FILE fileDownloadUri: "+fileDownloadUri);
			try (BufferedReader br = new BufferedReader(new FileReader(path_file+"/"+uripath))) {
			    
				String line;
			    List<HariMerah> resp =  new ArrayList<HariMerah>();
			    
			    while ((line = br.readLine()) != null) {
			    	String clean = line.replaceAll("\\P{Print}", ""); //ini perintah untuk remove unicode
			        String[] values = clean.split("\\,");
			        
			        String nomor = values[0];
			        String tentang = values[1];
			        String description = values[2];
			        String startdate = values[3];
			        String enddate = values[4];
			        String kd = "";
			        
			        logger.info("no : "+nomor);
					logger.info("tentang : "+tentang);
					logger.info("description : "+description);
					logger.info("startdate : "+startdate);
					logger.info("enddate : "+enddate);
					
					configInputDate.setTimeZone(TimeZone.getTimeZone("UTC"));
					Date date1 = configInputDate.parse(startdate);
					Date date2 = configInputDate.parse(enddate);
			
					HariMerah last = serviceHari.findLast();
					
					if(last == null) {
						kd = "LB00001";
					} else {
						int no = Integer.parseInt(last.getKode().substring(2));
						no = no + 1;
						
						if(String.valueOf(no).length() == 5 ) {
							kd = "LB" + no;
						} else if(String.valueOf(no).length() == 4 ) {
							kd = "LB0" + no;
						} else if(String.valueOf(no).length() == 3 ) {
							kd = "LB00" + no;
						} else if(String.valueOf(no).length() == 2 ) {
							kd = "LB000" + no;
						} else {
							kd = "LB0000" + no;
						}
					}
					
					int durasi = calculateNumberOfDaysBetween(date1, date2);
					logger.info("durasi start and end : "+durasi);
					
					for (int i = 0; i < durasi; i++) {
						
						Calendar c = Calendar.getInstance(); 
						c.setTime(date1); 
						c.add(Calendar.DATE, i);
						
						HariMerah createHari = new HariMerah();
						createHari.setKode(kd);
						createHari.setTentang(tentang);
						createHari.setDescription(description);
						createHari.setTanggal(c.getTime());
						this.serviceHari.save(createHari);
						
						resp.add(createHari);
					}
			    }
			    
			    ResponList data = new ResponList();
				data.setCode("1");
				data.setData(resp);
				return new ResponseEntity(data,HttpStatus.OK);
			    
			} catch (Exception e) {
				// TODO: handle exception
				logger.error(e.toString());
				logger.error(this.getClass().toGenericString(), e);
			}
			
			File filelama = new File(path_file+"/"+uripath);	
			filelama.delete();							
				
			return new ResponseEntity(responC.customRespon("200", "RKA berhasil dibuat .."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
		}
		return new ResponseEntity<String> ("Exception: Server Error",HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	public int calculateNumberOfDaysBetween(Date startDate, Date endDate) {
	    if (startDate.after(endDate)) {
	        throw new IllegalArgumentException("End date should be grater or equals to start date");
	    }

	    long startDateTime = startDate.getTime();
	    long endDateTime = endDate.getTime();
	    long milPerDay = 1000*60*60*24; 

	    int numOfDays = (int) ((endDateTime - startDateTime) / milPerDay); // calculate vacation duration in days

	    return ( numOfDays + 1); // add one day to include start date in interval
	}
}
