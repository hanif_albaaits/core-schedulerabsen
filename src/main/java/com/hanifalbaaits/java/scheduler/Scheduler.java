package com.hanifalbaaits.java.scheduler;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hanifalbaaits.java.controller.SchedullerController;
import com.hanifalbaaits.java.model.HariMerah;
import com.hanifalbaaits.java.service.HariMerahService;

@Component
public class Scheduler {
	
	private static Logger logger = LoggerFactory.getLogger(Scheduler.class);
	
	@Autowired
	SchedullerController controller;
	
	@Autowired
	HariMerahService serviceHari;
	
	//@Scheduled(cron="0 30 5 */1 * *") //setiap 1 mingguX
	@Scheduled(cron="0 0 5 ? * MON-FRI")	//seminggu X hari kerja
	public void createNewAbsen() throws Exception,ParseException {
		try {
			
			logger.info("CRONJOB SCHEDULE");
			Date today = new Date();
			logger.info("CRONJOB SCHEDULE - today : "+today.toString());
			logger.info("CRONJOB SCHEDULE - cekTabel");
			List<HariMerah> libur = serviceHari.findByToday(today);
			if(libur.size() > 0) {
				logger.info("CRONJOB SCHEDULE - hari ini libur");
				for (HariMerah hariMerah : libur) {
					logger.info("CRONJOB SCHEDULE - kode: "+hariMerah.getKode());
					logger.info("CRONJOB SCHEDULE - tentang : "+hariMerah.getTentang());
					logger.info("CRONJOB SCHEDULE - tanggal: "+hariMerah.getTanggal());
				}
				logger.info("CRONJOB SCHEDULE - tidak dibuat create absen");
				
			} else {
				
				logger.info("CRONJOB SCHEDULE - bukan hari libur");
				logger.info("CRONJOB SCHEDULE - buat create absen");
				controller.createNewAbsen();
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
		}
		
	}
	
	@Scheduled(cron="0 30 5 */1 * *")
	public void checkAbsenLembur() throws ParseException, Exception {
		try {
			
			logger.info("cronjob - absenLembur");
			controller.checkAbsenLembur();
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.toString());
			logger.error(this.getClass().toGenericString(), e);	
		}
		
	}
	
	//@Scheduled(cron="*/5 * * * * *")
	public void testSCheduler() throws ParseException, Exception {
		logger.info("info");
		logger.debug("debug");
		logger.trace("trace");
		logger.error("error");
	}
}