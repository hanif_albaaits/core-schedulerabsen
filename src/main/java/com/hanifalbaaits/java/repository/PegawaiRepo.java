package com.hanifalbaaits.java.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.model.Pegawai;

@Repository
public interface PegawaiRepo extends JpaRepository<Pegawai, Integer>{
	
	@Query("select v from Pegawai v")
	List<Pegawai> findAll();
	
	@Query("select v from Pegawai v where v.status=1")
	List<Pegawai> findAllActive();
	
	@Query("select v from Pegawai v where v.id=?1 or v.nik=?1 or v.address_uid=?1 or v.uid=?1")
	List<Pegawai> findPegawai(String param);
	
	@Query("select v from Pegawai v where v.status=1 and v.nik=?1")
	List<Pegawai> findAllActiveByNIK(String nik);
}
