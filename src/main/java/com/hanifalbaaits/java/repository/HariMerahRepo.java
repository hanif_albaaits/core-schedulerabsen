package com.hanifalbaaits.java.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.model.HariMerah;

@Repository
public interface HariMerahRepo extends JpaRepository<HariMerah, Integer>{
	
	@Query("select v from HariMerah v")
	List<HariMerah> findAll();
	
	@Query("select v from HariMerah v where v.id = ?1")
	HariMerah findById(int id);
	
	@Query("select v from HariMerah v where v.kode = ?1")
	List<HariMerah> findByKode(String kode);
	
	@Query(
			  value = "select * from tb_harimerah order by id desc limit 1", 
			  nativeQuery = true)
	HariMerah findLast();
	
//	@Query(
//			  value = "select * from tb_harimerah where tanggal>= ?1 and tanggal <= ?2", 
//			  nativeQuery = true)
	@Query("select v from HariMerah v where v.tanggal >= ?1 and v.tanggal <= ?2")
	List<HariMerah> findByDate(Date start, Date end);
	
	@Query("select v from HariMerah v where v.tanggal >= ?1 and v.tanggal <= ?2 group by v.kode")
	List<HariMerah> grupByKodeAndDate(Date start, Date end);
	
	@Query("select v from HariMerah v where v.tanggal >= ?1 group by v.kode order by v.tanggal")
	List<HariMerah> grupByKodeAndStartDate(Date start);
	
//	@Query(
//			  value = "select * from tb_harimerah where tanggal = ?1", 
//			  nativeQuery = true)
	@Query("select v from HariMerah v where v.tanggal = ?1")
	List<HariMerah> findByToday(Date dt);
	
	@Modifying
	@Transactional
	@Query("delete from HariMerah where kode = ?1")
	void deleteByKode(String kode);
}
