package com.hanifalbaaits.java.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.model.Absen;

@Repository
public interface AbsenRepo extends JpaRepository<Absen, Integer>{

	@Query("select v from Absen v where v.id=?1 and v.tanggal_in=?2")
	Absen findAbsenDate1(int id, Date tanggal);
	
	@Query(nativeQuery=true, 
			value="select * from tb_absen where id_pegawai=?1 and tanggal_in=?2")
	Absen findAbsenDate(int id, Date tanggal);
}
