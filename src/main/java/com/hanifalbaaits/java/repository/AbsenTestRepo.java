package com.hanifalbaaits.java.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.model.AbsenTest;

@Repository
public interface AbsenTestRepo extends JpaRepository<AbsenTest, Integer>{

	@Query("select v from AbsenTest v where v.id=?1 and v.tanggal_in=?2")
	AbsenTest findAbsenDate1(int id, Date tanggal);
	
	@Query(nativeQuery=true, 
			value="select * from tb_absen_test where id_pegawai=?1 and tanggal_in=?2")
	AbsenTest findAbsenDate(int id, Date tanggal);
}
