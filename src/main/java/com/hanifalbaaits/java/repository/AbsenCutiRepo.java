package com.hanifalbaaits.java.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.model.Absen;
import com.hanifalbaaits.java.model.AbsenCuti;

@Repository
public interface AbsenCutiRepo extends JpaRepository<AbsenCuti, Integer>{

	@Query(nativeQuery=true, 
			value="select * from tb_absen_cuti where id_pegawai=?1 and (tanggal_awal_cuti<=?2 and tanggal_akhir_cuti>=?2) and status_cuti=1")
	List<AbsenCuti> findCutiDate(int id, Date tgl);
}
