package com.hanifalbaaits.java.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.model.AbsenLembur;

@Repository
public interface AbsenLemburRepo extends JpaRepository<AbsenLembur, Integer>{

	@Query("select v from AbsenLembur v where v.id_lembur=1")
	List<AbsenLembur> findAbsenLembur();
	
	@Query(nativeQuery=true, 
			value="select * from tb_absen_lembur where tanggal_lembur=?1")
	List<AbsenLembur> findLemburYesterday(Date tanggal);
}
