package com.hanifalbaaits.java.client;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.hanifalbaaits.java.util.MyResponseErrorHandler;

@Component
@Configuration
public class ClientAPI {
	
	private static Logger logger = LoggerFactory.getLogger(ClientAPI.class);
	
	@Autowired
	RestTemplateBuilder builder;
	
	RestTemplate restTemplate;
	
	@Value("${url.api.absen}") //10.0.1.90/api_grtk/index.php/web_service/api
    String API;
	
	@Value("${key.api.absen}")
	String Key;
	
	String url = "http://10.0.1.90:8000/api/coresoyal/changecard";
	
	@PostConstruct
	public void init() {
		// TODO Auto-generated constructor stub
		this.restTemplate = this.builder.errorHandler(new MyResponseErrorHandler()).build();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String sendtoAbsen(String id, String nik, String nik_atasan){
	
		String url = "http://"+this.API+"/11001/0/0/LEMBUR_IN|0|"+id+"|"+nik+"|"+nik_atasan+"|0/"+this.Key;
        logger.info("URL "+url);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //headers.set("Authorization", Authorization);
        HttpEntity request = new HttpEntity(null, headers);
       
        ResponseEntity<String> response = restTemplate.exchange(url,
                        HttpMethod.GET, request, new ParameterizedTypeReference<String>() {
                        });
        return response.getBody();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String sendtoAbsen1(String id, String nik, String nik_atasan){

		String url = "http://"+this.API+"/11001/0/0/LEMBUR_OUT|0|"+id+"|"+nik+"|"+nik_atasan+"|0/"+this.Key;
        logger.info("URL "+url);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //headers.set("Authorization", Authorization);
        HttpEntity request = new HttpEntity(null, headers);
       
        ResponseEntity<String> response = restTemplate.exchange(url,
                        HttpMethod.GET, request, new ParameterizedTypeReference<String>() {
                        });
        return response.getBody();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String sendToChangecard(String address, String uid, String status){
		
		String jsonRequest = "{\n" +
				"    \"address\": \""+address+"\",\n" +
                "    \"uid\": \""+uid+"\",\n" +
                "    \"status\": \""+status+"\"\n" +
                "}"; 
		
		System.out.println(jsonRequest);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity request = new HttpEntity(jsonRequest,headers);
		
		logger.info("API: "+url);
		
		ResponseEntity<String> response = restTemplate.exchange(url, 
				HttpMethod.POST, request, new ParameterizedTypeReference<String>() {
		});
		return response.getBody();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String sendtoNewAbsen(String id, String nik) {
		// TODO Auto-generated method stub
		String url = "http://"+this.API+"/110012/0/0/LEMBUR|1|"+id+"|"+nik+"|"+nik+"|7|1/"+this.Key;
        logger.info("URL "+url);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //headers.set("Authorization", Authorization);
        HttpEntity request = new HttpEntity(null, headers);
       
        ResponseEntity<String> response = restTemplate.exchange(url,
                        HttpMethod.GET, request, new ParameterizedTypeReference<String>() {
                        });
        return response.getBody();
	}
}
