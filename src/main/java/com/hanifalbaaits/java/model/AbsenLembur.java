package com.hanifalbaaits.java.model;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_absen_lembur")
public class AbsenLembur {
	
	@Id
	@Column(name="id_lembur")
	private int id_lembur;
	
	@Column(name="id_pegawai")
	private int id_pegawai;
	
	@Column(name="tanggal_lembur")
	private Date tanggal_lembur;
	
	@Column(name="lembur_in")
	private Time lembur_in;
	
	@Column(name="lembur_out")
	private Time lembur_out;
	
	@Column(name="durasi")
	private String durasi;
	
	@Column(name="activity")
	private String activity;
	
	@Column(name="caption_in")
	private String caption_in;
	
	@Column(name="caption_out")
	private String caption_out;
	
	@Column(name="image_in")
	private String image_in;
	
	@Column(name="image_out")
	private String image_out;
	
	@Column(name="keterangan")
	private String keterangan;
	
	@Column(name="status_lemburin")
	private int status_lemburin;
	
	@Column(name="status_lemburout")
	private int status_lemburout;
	
	@Column(name="dilaporkan_kepada")
	private String lapor_kpd;
	
	@Column(name="nik")
	private String nik;
	
	@Column(name="nik_atasan")
	private String nik_atasan;

	public int getId_lembur() {
		return id_lembur;
	}

	public void setId_lembur(int id_lembur) {
		this.id_lembur = id_lembur;
	}

	public int getId_pegawai() {
		return id_pegawai;
	}

	public void setId_pegawai(int id_pegawai) {
		this.id_pegawai = id_pegawai;
	}

	public Date getTanggal_lembur() {
		return tanggal_lembur;
	}

	public void setTanggal_lembur(Date tanggal_lembur) {
		this.tanggal_lembur = tanggal_lembur;
	}

	public Time getLembur_in() {
		return lembur_in;
	}

	public void setLembur_in(Time lembur_in) {
		this.lembur_in = lembur_in;
	}

	public Time getLembur_out() {
		return lembur_out;
	}

	public void setLembur_out(Time lembur_out) {
		this.lembur_out = lembur_out;
	}

	public String getDurasi() {
		return durasi;
	}

	public void setDurasi(String durasi) {
		this.durasi = durasi;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getCaption_in() {
		return caption_in;
	}

	public void setCaption_in(String caption_in) {
		this.caption_in = caption_in;
	}

	public String getCaption_out() {
		return caption_out;
	}

	public void setCaption_out(String caption_out) {
		this.caption_out = caption_out;
	}

	public String getImage_in() {
		return image_in;
	}

	public void setImage_in(String image_in) {
		this.image_in = image_in;
	}

	public String getImage_out() {
		return image_out;
	}

	public void setImage_out(String image_out) {
		this.image_out = image_out;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public int getStatus_lemburin() {
		return status_lemburin;
	}

	public void setStatus_lemburin(int status_lemburin) {
		this.status_lemburin = status_lemburin;
	}

	public int getStatus_lemburout() {
		return status_lemburout;
	}

	public void setStatus_lemburout(int status_lemburout) {
		this.status_lemburout = status_lemburout;
	}

	public String getLapor_kpd() {
		return lapor_kpd;
	}

	public void setLapor_kpd(String lapor_kpd) {
		this.lapor_kpd = lapor_kpd;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNik_atasan() {
		return nik_atasan;
	}

	public void setNik_atasan(String nik_atasan) {
		this.nik_atasan = nik_atasan;
	}
}
