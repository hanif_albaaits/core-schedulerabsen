package com.hanifalbaaits.java.model;

public class HariMerahResult {
	
	private String kode;
	private String tentang;
	private String description;
	private String tanggal_mulai;
	private String tanggal_selesai;
	
	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}
	public String getTentang() {
		return tentang;
	}
	public void setTentang(String tentang) {
		this.tentang = tentang;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTanggal_mulai() {
		return tanggal_mulai;
	}
	public void setTanggal_mulai(String tanggal_mulai) {
		this.tanggal_mulai = tanggal_mulai;
	}
	public String getTanggal_selesai() {
		return tanggal_selesai;
	}
	public void setTanggal_selesai(String tanggal_selesai) {
		this.tanggal_selesai = tanggal_selesai;
	}
}
