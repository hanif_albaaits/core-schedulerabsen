package com.hanifalbaaits.java.model;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_absen")
public class Absen {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="id_pegawai")
	private int id_pegawai;
	
	@Column(name="tanggal_in")
	private Date tanggal_in;
	
	@Column(name="absen_in")
	private Time absen_in;
	
	@Column(name="ket_in")
	private String ket_in;
	
	@Column(name="tanggal_out")
	private Date tanggal_out;
	
	@Column(name="absen_out")
	private Time absen_out;
	
	@Column(name="ket_out")
	private String ket_out;
	
	@Column(name="keterangan")
	private String keterangan;
	
	@Column(name="image_in")
	private String image_in;
	
	@Column(name="image_out")
	private String image_out;
	
	@Column(name="caption_in")
	private String caption_in;
	
	@Column(name="caption_out")
	private String caption_out;
	
	@Column(name="latitude_in")
	private String latitude_in;
	
	@Column(name="latitude_out")
	private String latitude_out;
	
	@Column(name="longitude_in")
	private String longitude_in;
	
	@Column(name="longitude_out")
	private String longitude_out;
	
	@Column(name="status_in")
	private String status_in;
	
	@Column(name="status_out")
	private String status_out;
	
	public int getId_pegawai() {
		return id_pegawai;
	}
	public void setId_pegawai(int id_pegawai) {
		this.id_pegawai = id_pegawai;
	}
	public Date getTanggal_in() {
		return tanggal_in;
	}
	public void setTanggal_in(Date tanggal_in) {
		this.tanggal_in = tanggal_in;
	}
	public Time getAbsen_in() {
		return absen_in;
	}
	public void setAbsen_in(Time absen_in) {
		this.absen_in = absen_in;
	}
	public String getKet_in() {
		return ket_in;
	}
	public void setKet_in(String ket_in) {
		this.ket_in = ket_in;
	}
	public Date getTanggal_out() {
		return tanggal_out;
	}
	public void setTanggal_out(Date tanggal_out) {
		this.tanggal_out = tanggal_out;
	}
	public Time getAbsen_out() {
		return absen_out;
	}
	public void setAbsen_out(Time absen_out) {
		this.absen_out = absen_out;
	}
	public String getKet_out() {
		return ket_out;
	}
	public void setKet_out(String ket_out) {
		this.ket_out = ket_out;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getImage_in() {
		return image_in;
	}
	public void setImage_in(String image_in) {
		this.image_in = image_in;
	}
	public String getImage_out() {
		return image_out;
	}
	public void setImage_out(String image_out) {
		this.image_out = image_out;
	}
	public String getCaption_in() {
		return caption_in;
	}
	public void setCaption_in(String caption_in) {
		this.caption_in = caption_in;
	}
	public String getCaption_out() {
		return caption_out;
	}
	public void setCaption_out(String caption_out) {
		this.caption_out = caption_out;
	}
	public String getLatitude_in() {
		return latitude_in;
	}
	public void setLatitude_in(String latitude_in) {
		this.latitude_in = latitude_in;
	}
	public String getLatitude_out() {
		return latitude_out;
	}
	public void setLatitude_out(String latitude_out) {
		this.latitude_out = latitude_out;
	}
	public String getLongitude_in() {
		return longitude_in;
	}
	public void setLongitude_in(String longitude_in) {
		this.longitude_in = longitude_in;
	}
	public String getLongitude_out() {
		return longitude_out;
	}
	public void setLongitude_out(String longitude_out) {
		this.longitude_out = longitude_out;
	}
	public String getStatus_in() {
		return status_in;
	}
	public void setStatus_in(String status_in) {
		this.status_in = status_in;
	}
	public String getStatus_out() {
		return status_out;
	}
	public void setStatus_out(String status_out) {
		this.status_out = status_out;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
