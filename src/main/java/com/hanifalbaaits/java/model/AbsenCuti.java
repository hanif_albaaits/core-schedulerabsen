package com.hanifalbaaits.java.model;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_absen_cuti")
public class AbsenCuti {
	
	@Id
	@Column(name="id_cuti")
	private int id_cuti;
	
	@Column(name="id_pegawai")
	private int id_pegawai;
	
	@Column(name="tanggal_cuti")
	private Date tanggal_cuti;
	
	@Column(name="tanggal_awal_cuti")
	private Date tanggal_awal_cuti;
	
	@Column(name="tanggal_akhir_cuti")
	private Date tanggal_akhir_cuti;
	
	@Column(name="caption_cuti")
	private String caption_cuti;
	
	@Column(name="lokasi_cuti")
	private String lokasi_cuti;
	
	@Column(name="status_cuti")
	private int status_cuti;
	
	@Column(name="proses_cuti")
	private Time proses_cuti;
	
	public int getId_cuti() {
		return id_cuti;
	}
	public void setId_cuti(int id_cuti) {
		this.id_cuti = id_cuti;
	}
	public int getId_pegawai() {
		return id_pegawai;
	}
	public void setId_pegawai(int id_pegawai) {
		this.id_pegawai = id_pegawai;
	}
	public Date getTanggal_cuti() {
		return tanggal_cuti;
	}
	public void setTanggal_cuti(Date tanggal_cuti) {
		this.tanggal_cuti = tanggal_cuti;
	}
	public Date getTanggal_awal_cuti() {
		return tanggal_awal_cuti;
	}
	public void setTanggal_awal_cuti(Date tanggal_awal_cuti) {
		this.tanggal_awal_cuti = tanggal_awal_cuti;
	}
	public Date getTanggal_akhir_cuti() {
		return tanggal_akhir_cuti;
	}
	public void setTanggal_akhir_cuti(Date tanggal_akhir_cuti) {
		this.tanggal_akhir_cuti = tanggal_akhir_cuti;
	}
	public String getCaption_cuti() {
		return caption_cuti;
	}
	public void setCaption_cuti(String caption_cuti) {
		this.caption_cuti = caption_cuti;
	}
	public String getLokasi_cuti() {
		return lokasi_cuti;
	}
	public void setLokasi_cuti(String lokasi_cuti) {
		this.lokasi_cuti = lokasi_cuti;
	}
	public int getStatus_cuti() {
		return status_cuti;
	}
	public void setStatus_cuti(int status_cuti) {
		this.status_cuti = status_cuti;
	}
	public Time getProses_cuti() {
		return proses_cuti;
	}
	public void setProses_cuti(Time proses_cuti) {
		this.proses_cuti = proses_cuti;
	}
}
