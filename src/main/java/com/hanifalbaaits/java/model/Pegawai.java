package com.hanifalbaaits.java.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_pegawai")
public class Pegawai {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="nik")
	private String nik;
	
	@Column(name="nama_pegawai")
	private String nama;
	
	@Column(name="UID")
	private String uid;
	
	@Column(name="address_uid")
	private String address_uid;
	
	@Column(name="f_group")
	private int group;
	
	@Column(name="status")
	private int status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getAddress_uid() {
		return address_uid;
	}

	public void setAddress_uid(String address_uid) {
		this.address_uid = address_uid;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
